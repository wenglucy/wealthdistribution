package wealthDistribution;

import java.awt.geom.Point2D.Double;

/**
 * this class is to record all parameter in this system
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public class Parameter {
	// constant variables
	public static final int MAX_GRAIN = 50;
	public static final int MAP_SIZE = 51;
	public static final int ZERO = 0;
	public static final int MAX_INITIAL_WEALTH = 50;

	// the variables in the control panel
	public static int numPeople = 250;
	public static int maxVision = 5;
	public static int metabolismMax = 15;
	public static int lifeExpectancyMin = 1;
	public static int lifeExpectancyMax = 83;
	public static int percentBestLand = 25;
	public static int grainGrowthInterval = 1;
	public static int numGrainGrown = 10;

	// the direction
	public static Double north = new Double(0, 1);
	public static Double south = new Double(0, -1);
	public static Double west = new Double(-1, 0);
	public static Double east = new Double(1, 0);

	// the number of ticks
	public static final int TICKS = 2000;

}
