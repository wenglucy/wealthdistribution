package wealthDistribution;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * this class is to export the csv file
 *
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 */
public class ExportCSV {

	/**
	 * export the csv file this class is modified by the example from the web
	 * 
	 * @param dataList
	 *            the list of data that needs to export
	 * @param fileName
	 *            the csv file name
	 * @return
	 */
	public static boolean exportCsv(List<String> dataList, String fileName) {

		File file = new File(fileName);
		boolean isSuccess = false;

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;

		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out);
			bw = new BufferedWriter(osw);
			if (dataList != null && !dataList.isEmpty()) {
				for (String data : dataList) {
					bw.append(data).append("\r");
				}
			}

			isSuccess = true;
		} catch (Exception e) {
			isSuccess = false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return isSuccess;
	}

}