package wealthDistribution;

import java.util.Random;
import java.awt.geom.Point2D.Double;

/**
 * this class is responsible of recording all information of each turtle
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public class Turtle {
	// how old a turtle is
	private int age;
	// the amount of grain a turtle has
	private int wealth;
	// maximum age that a turtle can reach
	private int life_Expectancy;
	// how much grain a turtle eats each time
	private int metabolism;
	// how many patches ahead a turtle can see
	private int vision;
	// the position of the turtle
	private int position_x;
	private int position_y;
	// record the wealth class for each turtle
	private WealthClass wealthClass;
	// record the best direction that the turtle goes next step
	private Double best_Direction = Parameter.north;

	private Random r = new Random();

	/**
	 * set up a new turtle and initialize its values
	 */
	public Turtle() {
		moveToRandomPatch();
		setInitialTurtleVars();
		this.age = r.nextInt(this.life_Expectancy);
	}

	/**
	 * move turtle to a random patch
	 */
	public void moveToRandomPatch() {
		this.position_x = r.nextInt(Parameter.MAP_SIZE);
		this.position_y = r.nextInt(Parameter.MAP_SIZE);
	}

	/**
	 * Set up the initial values for the turtle variables
	 */
	public void setInitialTurtleVars() {

		this.age = 0;
		this.life_Expectancy = Parameter.lifeExpectancyMin
				+ r.nextInt(Parameter.lifeExpectancyMax - 
						Parameter.lifeExpectancyMin + 1);
		this.metabolism = 1 + r.nextInt(Parameter.metabolismMax);
		this.vision = 1 + r.nextInt(Parameter.maxVision);
		this.wealth = metabolism + r.nextInt(Parameter.MAX_INITIAL_WEALTH);
	}

	/**
	 * classify the turtle to the rich class
	 */
	public void setRich() {
		wealthClass = WealthClass.RICH;
	}

	/**
	 * classify the turtle to the middle class
	 */
	public void setMiddleClass() {
		wealthClass = WealthClass.MIDDLECLASS;
	}

	/**
	 * classify the turtle to the poor class
	 */
	public void setPoor() {
		wealthClass = WealthClass.POOR;
	}

	/**
	 * get the wealth class
	 * 
	 * @return wealthClass
	 */
	public WealthClass getWealthClass() {
		return wealthClass;
	}

	/**
	 * get the number of wealth
	 * 
	 * @return wealth
	 */
	public int getWealth() {
		return wealth;
	}

	/**
	 * 1.Determine the direction which is most profitable for each turtle
	 * in the surrounding patches within the turtles' vision
	 * 
	 * 2. Calculate the number of turtles on each patch
	 * 
	 * @param patches
	 */
	public void turnTowardsGrain(Patch[][] patches) {
		// towards north
		Double heading = Parameter.north;
		this.best_Direction = Parameter.north;
		int best_Amount = grainAhead(patches, heading);
		// towards east
		heading = Parameter.east;
		if (grainAhead(patches, heading) > best_Amount) {
			this.best_Direction = Parameter.east;
			best_Amount = grainAhead(patches, heading);
		}
		// towards south
		heading = Parameter.south;
		if (grainAhead(patches, heading) > best_Amount) {
			this.best_Direction = Parameter.south;
			best_Amount = grainAhead(patches, heading);
		}
		// towards west
		heading = Parameter.west;
		if (grainAhead(patches, heading) > best_Amount) {
			this.best_Direction = Parameter.west;
			best_Amount = grainAhead(patches, heading);
		}

		patches[this.position_x][this.position_y].addTurtle();
		if(this.wealthClass == WealthClass.RICH){
			patches[this.position_x][this.position_y].addRichTurtle();
		}
		if(this.wealthClass == WealthClass.POOR){
			patches[this.position_x][this.position_y].addPoorTurtle();
		}
		if(this.wealthClass == WealthClass.MIDDLECLASS){
			patches[this.position_x][this.position_y].addPoorTurtle();
		}
	}

	/**
	 * Calculate the total amount of grains in a direction
	 * 
	 * @param patches
	 * @param heading
	 *            the direction that the turtle faces
	 * @return total the total grain within the turtle's vision
	 */
	public int grainAhead(Patch[][] patches, Double heading) {
		int total = 0;
		int x = 0;
		int y = 0;
		// add all grain within the vision
		for (int i = 0; i < this.vision; i++) {
			x = (int) (position_x + (i + 1) * heading.x);
			y = (int) (position_y + (i + 1) * heading.y);
			// wrap the map, if neighbors are out of boundary
			x = boundaryWrap(x);
			y = boundaryWrap(y);

			total = total + (int) patches[x][y].getGrainHere();

		}
		return total;

	}

	/**
	 * Each turtle harvests the grain on its patch. If there are multiple
	 * turtles on a patch, divide the grain evenly among the turtles
	 * 
	 * @param patches
	 */
	public void harvestTurtle(Patch[][] patches) {
		int x = (int) this.position_x;
		int y = (int) this.position_y;
		this.wealth += Math.floor((double) patches[x][y].getGrainHere() /
				patches[x][y].getTurtlesHere());
	}

	/**
	 * Now that the grain has been harvested, have the turtles make the patches
	 * which they are on have no grain
	 * 
	 * @param patches
	 */
	public void clearPatch(Patch[][] patches) {
		patches[this.position_x][this.position_y].clearTurtle();
		patches[this.position_x][this.position_y].setGrainHere(0);
	}

	/**
	 * The turtle moves towards the best direction, then consumes grains and
	 * grows older. It will die or reborn if it has no grain or it is older 
	 * than the life expectancy
	 */
	public void moveEatAgeDie() {
		// Move towards the best direction
		this.position_x += this.best_Direction.x;
		this.position_y += this.best_Direction.y;
		// wrap the map, if neighbors are out of boundary
		this.position_x = boundaryWrap((int) this.position_x);
		this.position_y = boundaryWrap((int) this.position_y);

		// Consume some grain according to metabolism
		this.wealth = this.wealth - this.metabolism;
		// Grow older
		this.age++;
		// Check for death conditions: if the turtle has no grain or
		// it is older than the life expectancy or if some random factor holds,
		// then the turtle "dies" and is "reborn"
		// (in fact, it's variables are just reset to new random values)
		if (this.wealth < 0 || this.age > this.life_Expectancy) {
			System.out.println(this.age+", "+ this.life_Expectancy+", "+this.metabolism+", "+
		        this.wealthClass +", "+this.wealth);
			setInitialTurtleVars();
		}
	}

	/**
	 * if position x is out of boundary, wrap around the matrix.
	 * 
	 * @param x
	 * @return
	 */
	private static int boundaryWrap(int x) {
		if (x < 0) {
			x %= Parameter.MAP_SIZE + 1;
			x += Parameter.MAP_SIZE;
		}
		if (x >= Parameter.MAP_SIZE) {
			x %= Parameter.MAP_SIZE;
		}

		return x;

	}

	public int getPosition_x() {
		return this.position_x;
	}

	public int getPosition_y() {
		return this.position_y;
	}
	
    public void changeVersion(int maxWealth){
		
		if(this.wealth == maxWealth){
			this.vision = Parameter.maxVision;
		}else{
			this.vision = 
					(int)((double)this.wealth/maxWealth*Parameter.maxVision+1);
		}
	}

}
