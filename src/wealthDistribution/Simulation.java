package wealthDistribution;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * this class is responsible of the following functions: 1. building the whole
 * world 2. running the world based on discrete time 3. store and export the
 * data
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public class Simulation {

	// the parts of the system
	private static Patch[][] patches;
	private static Turtle[] turtles;
	private static double[][] update;
	// the data of the system
	private static List<String> tickDataList = new ArrayList<String>();
	private static List<String> lorenzDataList = new ArrayList<String>();
	private static String storePath = "D:/test";

	public static void main(String[] args) {

		// set up the initial patches of the world
		setupPatches();
		// set up the turtles
		setupTurtles();
		// create a TickData to store export data
		TickData td = new TickData(turtles);
		// store the data once the system builds
		tickDataList.add(td.tickData());
		// run the system
		for (int tick = 1; tick <= Parameter.TICKS; tick++) {
			go(tick);
			// after each running, store once data
			tickDataList.add(td.tickData());
			// only store the last point of data to calculate Lorenz data
			if (tick == 2000) {
				lorenzDataList = td.getLorenzData();
//				System.out.println("the number of turtle on each patch");
//				for (int i = 0; i< Parameter.MAP_SIZE;i++){
//				for (int j = 0; j< Parameter.MAP_SIZE;j++){
//					 int[] patchTurtle = new int[4];
//					 patchTurtle[0] = (int) patches[i][j].getGrainBefore();
//					 String str1 = new DecimalFormat("000").format(patchTurtle[0]);
//					 patchTurtle[1] = patches[i][j].getRichTurtlesHere();
//					 String str2 = new DecimalFormat("000").format(patchTurtle[1]);
//					 patchTurtle[2] = patches[i][j].getMiddleTurtlesHere();
//					 String str3 = new DecimalFormat("000").format(patchTurtle[2]);
//					 patchTurtle[3] = patches[i][j].getPoorTurtlesHere();
//					 String str4 = new DecimalFormat("000").format(patchTurtle[3]);
//		        //if(patchTurtle[1]+patchTurtle[2]+patchTurtle[3]!=0){     
//                    System.out.print("("+str1+", "+str2+", "+str3+", "+str4+") ");		
//		        //}
//				}
//				System.out.println();
//				}
			}
			
			for (int i = 0; i< Parameter.MAP_SIZE;i++){
			for (int j = 0; j< Parameter.MAP_SIZE;j++){
				 patches[i][j].clearPatchTurtle();
			}
			}
		}
		// create a directory if the directory doesn't exist
		MakeDirectory();
		// export the data
		ExportCSV.exportCsv(tickDataList, storePath + 
				"/E1_highResource_tickDataList_3.csv");
		ExportCSV.exportCsv(lorenzDataList, storePath + 
				"/E1_highResource_LorenzDataList_3.csv");
	}

	/**
	 * Set up initial variables for each turtle
	 */
	public static void setupTurtles() {
		// create specified number of the turtle in the system
		turtles = new Turtle[Parameter.numPeople];
		for (int i = 0; i < Parameter.numPeople; i++) {
			// initial the parameter of each turtle
			turtles[i] = new Turtle();
		}
		setTurtlesWealthClass();
	}

	/**
	 * Set the class of the turtles
	 */
	public static void setTurtlesWealthClass() {
		int max_Wealth = 0;
		// Calculate the wealth of richest turtle
		for (int i = 0; i < Parameter.numPeople; i++) {
			if (turtles[i].getWealth() > max_Wealth) {
				max_Wealth = turtles[i].getWealth();
			}
		}

		// If a turtle has less than a third the wealth of the richest turtle,
		// set as poor.
		// If between one and two thirds, set as middle.
		// If over two thirds, set as rich.
		for (int j = 0; j < Parameter.numPeople; j++) {
			
			turtles[j].changeVersion(max_Wealth);
			
			if (turtles[j].getWealth() <= (max_Wealth / 3)) {
				turtles[j].setPoor();
			} else if (turtles[j].getWealth() <= (max_Wealth * 2 / 3)) {
				turtles[j].setMiddleClass();
			} else {
				turtles[j].setRich();
			}
		}
	}

	/**
	 * The whole system goes for a tick
	 * 
	 * @param tick
	 */
	public static void go(int tick) {

		// Choose direction holding most grain within the turtle's vision
		for (int i = 0; i < Parameter.numPeople; i++) {
			turtles[i].turnTowardsGrain(patches);
		}
		
//		 System.out.println("the number of turtle on each patch");
//		 for (int i = 0; i< Parameter.MAP_SIZE;i++){
//		 for (int j = 0; j< Parameter.MAP_SIZE;j++){
//			 int[] patchTurtle = new int[4];
//			 patchTurtle[0] = patches[i][j].getTurtlesHere();
//			 patchTurtle[1] = patches[i][j].getRichTurtlesHere();
//			 patchTurtle[2] = patches[i][j].getMiddleTurtlesHere();
//			 patchTurtle[3] = patches[i][j].getPoorTurtlesHere();
//			 
//		 }
//		 }	
		 // System.out.print(patches[i][j].getTurtlesHere()+" ");
		// }
		// System.out.println();
		// }
		// System.out.println();
		//
		// System.out.println("the number of grain on each patch");
		// for (int i = 0; i< Parameter.MAP_SIZE;i++){
		// for (int j = 0; j< Parameter.MAP_SIZE;j++){
		// System.out.print((int)patches[i][j].getGrainHere()+" ");
		// }
		// System.out.println();
		// }
		// System.out.println();
		//
		// System.out.println("the max number of grain here on each patch");
		// for (int i = 0; i< Parameter.MAP_SIZE;i++){
		// for (int j = 0; j< Parameter.MAP_SIZE;j++){
		// System.out.print((int)patches[i][j].getMaxGrainHere()+" ");
		// }
		// System.out.println();
		// }
		// System.out.println();
		//
		for (int i = 0; i< Parameter.MAP_SIZE;i++){
		for (int j = 0; j< Parameter.MAP_SIZE;j++){
			patches[i][j].setGrainBefore();
		}
		}
		
		// Have turtles harvest before any turtle sets the patch to 0
		for (int i = 0; i < Parameter.numPeople; i++) {
			turtles[i].harvestTurtle(patches);
		}

		// Have turtles harvest before any turtle sets the patch to 0
		for (int i = 0; i < Parameter.numPeople; i++) {
			int x = turtles[i].getPosition_x();
			int y = turtles[i].getPosition_y();
			patches[x][y].clearGrainHere();
			patches[x][y].clearTurtle();
		}

		// All turtles go
		for (int i = 0; i < Parameter.numPeople; i++) {
			turtles[i].moveEatAgeDie();
		}

		// Set the class of the turtles again
		setTurtlesWealthClass();

		// Grow grain every grain-growth-interval clock ticks
		if ((tick % Parameter.grainGrowthInterval) == 0) {
			for (int i = 0; i < Parameter.MAP_SIZE; i++) {
				for (int j = 0; j < Parameter.MAP_SIZE; j++) {
					patches[i][j].growGrain(Parameter.numGrainGrown);
				}
			}
		}

	}

	/**
	 * Set up the initial amounts of grain each patch has
	 */
	public static void setupPatches() {
		patches = new Patch[Parameter.MAP_SIZE][Parameter.MAP_SIZE];
		update = new double[Parameter.MAP_SIZE][Parameter.MAP_SIZE];

		// create patches
		for (int i = 0; i < Parameter.MAP_SIZE; i++) {
			for (int j = 0; j < Parameter.MAP_SIZE; j++) {

				patches[i][j] = new Patch(i, j);
				update[i][j] = 0;

				// Give some patches the highest amount of grain possible --
				// these patches are the "best land"
				if (Math.random() * 100 <= Parameter.percentBestLand) {
					patches[i][j].setMaxGrainHere(Parameter.MAX_GRAIN);
					patches[i][j].setGrainHere(
							patches[i][j].getMaxGrainHere());
				} else {
					patches[i][j].setGrainHere(0);
					patches[i][j].setMaxGrainHere(0);
				}
			}
		}

		// Spread that grain around the window a little and put a little back
		// into the patches that are the "best land" found above
		diffuse(5, 0.25, true);
		// Spread the grain around some more
		diffuse(10, 0.25, false);

		// Set the max-grain-here for each patch after diffusion
		for (int i = 0; i < Parameter.MAP_SIZE; i++) {
			for (int j = 0; j < Parameter.MAP_SIZE; j++) {
				// Round grain levels to whole numbers
				patches[i][j].setGrainHere(
						Math.floor(patches[i][j].getGrainHere()));
				// Initial grain level is also maximum
				patches[i][j].setMaxGrainHere(patches[i][j].getGrainHere());
			}
		}

		int total_grain = 0;
		// Set the max-grain-here for each patch after diffusion
		for (int i = 0; i < Parameter.MAP_SIZE; i++) {
			for (int j = 0; j < Parameter.MAP_SIZE; j++) {
				total_grain += patches[i][j].getGrainHere();
			}
		}
		//System.out.println("the total grain is " + total_grain);

	}

	/**
	 * tells each patch to give equal shares of (number * 100) percent of the
	 * value of patch-variable to its eight neighboring patches
	 * 
	 * @param repeatNum
	 *            the number of repeat
	 * @param diffuseRate
	 *            the percent of the value of patch wealth to diffuse
	 * @param isResetGrainHere
	 *            whether to reset the grain here
	 */
	private static void diffuse(int repeatNum, double diffuseRate,
			boolean isResetGrainHere) {

		for (int k = 0; k < repeatNum; k++) {
			for (int i = 0; i < Parameter.MAP_SIZE; i++) {
				for (int j = 0; j < Parameter.MAP_SIZE; j++) {

					// if isResentGrainHere is true, reset the grain_here to
					// max_grain_here
					if (isResetGrainHere) {
						if (patches[i][j].getMaxGrainHere() != 0) {
							patches[i][j].setGrainHere(
									patches[i][j].getMaxGrainHere());
						}
					}

					double numGrain = patches[i][j].getGrainHere();
					// Diffuse equal shares of the value to 8 neighbors.
					double neighborsGrain = numGrain * diffuseRate * 0.125;

					for (int m = i - 1; m < i + 2; m++) {
						for (int n = j - 1; n < j + 2; n++) {
							// wrap the map, if neighbors are out of boundary
							int x = boundaryWrap(m);
							int y = boundaryWrap(n);

							if (x != i || y != j) {
								update[x][y] += neighborsGrain;
							}

						}
					}
					update[i][j] -= numGrain * diffuseRate;
				}
			}
			// change the grain of each patch after one diffusion and reset the
			// variant to zero
			for (int i = 0; i < Parameter.MAP_SIZE; i++) {
				for (int j = 0; j < Parameter.MAP_SIZE; j++) {
					patches[i][j].addGrainHere(update[i][j]);
					update[i][j] = 0;

				}
			}
		}
	}

	/**
	 * if position x is out of boundary, wrap around the matrix.
	 * 
	 * @param x
	 * @return
	 */
	private static int boundaryWrap(int x) {
		if (x < 0) {
			x %= Parameter.MAP_SIZE + 1;
			x += Parameter.MAP_SIZE;
		}
		if (x >= Parameter.MAP_SIZE) {
			x %= Parameter.MAP_SIZE;
		}

		return x;

	}

	/**
	 * Generate a directory to store generated csv files
	 */
	private static void MakeDirectory() {
		File file = new File(storePath);
		// judge whether the directory exists.
		// if not, create one.
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created");
			} else {
				System.out.println("Not create directory");
			}
		}
	}

}
