package wealthDistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * this class is to calculate and store the data in this system, including the
 * number of the rich turtles, middle turtles, poor turtles, Lorenz and gini
 * index
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public class TickData {

	// create variables to store all data
	private Turtle[] turtles;
	private int[] turtlesWealth;
	private double[] wealthSumSoFar;
	private static int richNum;
	private static int middleNum;
	private static int poorNum;
	private static double giniIndex;
	private int totalWealth;

	/**
	 * initialize the data
	 * 
	 * @param turtles
	 */
	public TickData(Turtle[] turtles) {
		this.turtles = turtles;
		turtlesWealth = new int[turtles.length];
		wealthSumSoFar = new double[turtles.length];
		richNum = 0;
		middleNum = 0;
		poorNum = 0;
		totalWealth = 0;
	}

	/**
	 * Calculate the number of turtles in this wealth class, then sort the
	 * wealths of turtles
	 */
	public void sortTurtlesWealth() {
		totalWealth = 0;
		for (int i = 0; i < turtles.length; i++) {

			turtlesWealth[i] = turtles[i].getWealth();
			totalWealth = totalWealth + turtlesWealth[i];

			if (turtles[i].getWealthClass().equals(WealthClass.RICH)) {
				this.richNum++;
			} else if (turtles[i].getWealthClass().equals(
					WealthClass.MIDDLECLASS)) {
				this.middleNum++;
			} else if (turtles[i].getWealthClass().equals(
					WealthClass.POOR)) {
				this.poorNum++;
			} else {
				System.out.println("WealthClass Wrong");
			}
		}

		Arrays.sort(turtlesWealth);
		// System.out.println(totalWealth);
	}

	/**
	 * Calculate the accumulated wealth so far in lorenz-points
	 */
	public void drawLorenz() {

		this.wealthSumSoFar[0] = (double) turtlesWealth[0] /
				this.totalWealth * 100;

		for (int i = 1; i < turtles.length; i++) {
			this.wealthSumSoFar[i] = (double) turtlesWealth[i] / 
					this.totalWealth * 100 + this.wealthSumSoFar[i - 1];
		}
	}

	/**
	 * Calculate the Gini index
	 */
	public void calculateGini() {

		this.giniIndex = 0;
		for (int index = 1; index <= turtles.length; index++) {
			this.giniIndex += ((double) index / turtles.length)
					- ((double) wealthSumSoFar[index - 1] / 
							wealthSumSoFar[turtles.length - 1]);
		}
		this.giniIndex = (this.giniIndex / turtles.length) / 0.5;
	}

	/**
	 * Compute the number of turtles for each wealth class, Lorenz values and
	 * Gini-Index for each tick
	 * 
	 * @return tickDataString
	 */
	public String tickData() {
		String tickDataString = null;
		sortTurtlesWealth();
		drawLorenz();
		calculateGini();
		tickDataString = poorNum + "," + middleNum + "," + richNum + 
				"," + giniIndex;

		//System.out.println(tickDataString);
		clearClass();
		return tickDataString;
	}

	/**
	 * clear statistical result of the wealth class
	 */
	public void clearClass() {
		this.richNum = 0;
		this.middleNum = 0;
		this.poorNum = 0;
	}

	/**
	 * Store all the Lorenz-points as the Lorenz data list
	 * 
	 * @return lorenzDataList
	 */
	public List<String> getLorenzData() {
		List<String> lorenzDataList = new ArrayList<String>();
		String wealthSumSoFarString = null;
		double lorenzNo = 0;
		for (int i = 0; i < turtles.length; i++) {
			lorenzNo = (double) (i + 1) / Parameter.numPeople * 100;
			wealthSumSoFarString = lorenzNo + "," + wealthSumSoFar[i];
			lorenzDataList.add(wealthSumSoFarString);
		}
		return lorenzDataList;
	}
}
