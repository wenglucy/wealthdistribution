package wealthDistribution;
/**
 * this class is to classify the wealth class for each turtle
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public enum WealthClass {
    RICH, MIDDLECLASS, POOR
}
