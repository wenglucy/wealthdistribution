package wealthDistribution;

import java.awt.geom.Point2D.Double;

/**
 * this class is responsible of recording all information of each patch
 * 
 * @author Qiulei Zhang, Luxin Weng, Ruoyi Wang
 *
 */
public class Patch {
	// the current amount of grain on this patch
	private double grain_here;
	// the maximum amount of grain this patch can hold
	private double max_grain_here;
	// the position of the patch
	private Double position = new Double();
	// the number of the turtles in this patch
	private int turtles_here;
	
	private int poor_turtles_here;
	private int middle_turtles_here;
	private int rich_turtles_here;
    private int grain_before;

	/**
	 * get the position of the patch in the world
	 */
	public Patch(int x, int y) {
		this.position.x = x;
		this.position.y = y;
	}
	
	public void setGrainBefore(){
		this.grain_before = (int) this.grain_here;
	}
	
	public int getGrainBefore(){
		return this.grain_before;
	}

	/**
	 * grow the grain here
	 * 
	 * @param grainHere
	 *            the number of growth each time
	 */
	public void growGrain(double grainHere) {
		// the grain here grows when the grain here hasn't reached the maximum
		if (grain_here < max_grain_here) {
			grain_here += grainHere;
			if (grain_here > max_grain_here) {
				grain_here = max_grain_here;
			}
		}
	}

	/**
	 * get the number of the grain_here
	 * 
	 * @return grain_here
	 */
	public double getGrainHere() {
		return this.grain_here;
	}

	/**
	 * set the number of grain_here
	 * 
	 * @param grainHere
	 */
	public void setGrainHere(double grainHere) {
		this.grain_here = grainHere;
	}

	public void clearGrainHere() {
		this.grain_here = 0;
	}

	/**
	 * add the number of grain
	 * 
	 * @param grain
	 */
	public void addGrainHere(double grain) {
		this.grain_here += grain;
	}

	/**
	 * get the number of the maximum of grain_here
	 * 
	 * @return max_grain_here
	 */
	public double getMaxGrainHere() {
		return this.max_grain_here;
	}

	/**
	 * set the number of the maximum of grain_here
	 * 
	 * @param maxGrain
	 */
	public void setMaxGrainHere(double maxGrain) {
		this.max_grain_here = maxGrain;
	}

	/**
	 * get the position of the patch
	 * 
	 * @return position
	 */
	public Double getPosition() {
		return this.position;
	}

	/**
	 * get the number of turtles in this patch currently
	 * 
	 * @return turtles_here
	 */
	public int getTurtlesHere() {
		return this.turtles_here;
	}
	
	public int getPoorTurtlesHere() {
		return this.poor_turtles_here;
	}
	
	public int getRichTurtlesHere() {
		return this.rich_turtles_here;
	}
	
	public int getMiddleTurtlesHere() {
		return this.middle_turtles_here;
	}

	public void clearPatchTurtle(){
		this.poor_turtles_here=0;
		this.rich_turtles_here=0;
		this.middle_turtles_here=0;
	}
	/**
	 * add one turtle in the patch
	 */
	public void addTurtle() {
		this.turtles_here++;
	}
	
	public void addPoorTurtle(){
		this.poor_turtles_here++;
	}

	public void addMiddleTurtle(){
		this.middle_turtles_here++;
	}
	
	public void addRichTurtle(){
		this.rich_turtles_here++;
	}
	/**
	 * clear the number of turtle in this patch
	 */
	public void clearTurtle() {
		this.turtles_here = 0;
	}
}
